" ===============================================================
" Luciano
" A dark color scheme for vim
" URL: https://antenore.simbiosi.org/Luciano
" Author: Antenore Gatta
" License: MIT
" Last Change: 2021/03/29 16:13
" ===============================================================

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name="Luciano"


let Italic = ""
if exists('g:Luciano_italic')
  let Italic = "italic"
endif
let g:Luciano_italic = get(g:, 'Luciano_italic', 0)

let Bold = ""
if exists('g:Luciano_bold')
  let Bold = "bold"
endif

let g:Luciano_bold = get(g:, 'Luciano_bold', 0)
hi ColorColumn guifg=NONE ctermfg=NONE guibg=#323232 ctermbg=236 gui=NONE cterm=NONE
hi CursorColumn guifg=NONE ctermfg=NONE guibg=#323232 ctermbg=236 gui=NONE cterm=NONE
hi CursorLine guifg=NONE ctermfg=NONE guibg=#323232 ctermbg=236 gui=NONE cterm=NONE
hi CursorLineNr guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Directory guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi DiffAdd guifg=NONE ctermfg=NONE guibg=#2d7072 ctermbg=6 gui=NONE cterm=NONE
hi DiffChange guifg=NONE ctermfg=NONE guibg=#23375c ctermbg=237 gui=NONE cterm=NONE
hi DiffDelete guifg=#f91212 ctermfg=9 guibg=#c3193e ctermbg=125 gui=NONE cterm=NONE
hi DiffText guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=reverse cterm=reverse
hi ErrorMsg guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=reverse cterm=reverse
hi VertSplit guifg=#04040c ctermfg=232 guibg=#04040c ctermbg=232 gui=NONE cterm=NONE
hi Folded guifg=#647b85 ctermfg=66 guibg=#2c434d ctermbg=238 gui=NONE cterm=NONE
hi FoldColumn guifg=#647b85 ctermfg=66 guibg=#2c434d ctermbg=238 gui=NONE cterm=NONE
hi SignColumn guifg=#0c0b0c ctermfg=232 guibg=#04040c ctermbg=232 gui=NONE cterm=NONE
hi IncSearch guifg=#ffffff ctermfg=15 guibg=#000000 ctermbg=0 gui=Bold,reverse cterm=Bold,reverse
hi LineNr guifg=#344651 ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi MatchParen guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi NonText guifg=#344651 ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Normal guifg=#fbfbe6 ctermfg=230 guibg=#04040c ctermbg=232 gui=NONE cterm=NONE
hi PMenu guifg=#a7d9e1 ctermfg=152 guibg=#23375c ctermbg=237 gui=NONE cterm=NONE
hi PMenuSel guifg=#23375c ctermfg=237 guibg=#52dcdc ctermbg=80 gui=NONE cterm=NONE
hi PmenuSbar guifg=#23375c ctermfg=237 guibg=#23375c ctermbg=237 gui=NONE cterm=NONE
hi PmenuThumb guifg=#52dcdc ctermfg=80 guibg=#52dcdc ctermbg=80 gui=NONE cterm=NONE
hi Question guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Search guifg=#ffffff ctermfg=15 guibg=NONE ctermbg=NONE gui=underline,Bold cterm=underline,Bold
hi SpecialKey guifg=#344651 ctermfg=238 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellBad guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellLocal guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellCap guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi SpellRare guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi StatusLine guifg=#242e33 ctermfg=236 guibg=#647b85 ctermbg=66 gui=Bold cterm=Bold
hi StatusLineNC guifg=#0c0b0c ctermfg=232 guibg=#344651 ctermbg=238 gui=NONE cterm=NONE
hi TabLine guifg=#0c0b0c ctermfg=232 guibg=#344651 ctermbg=238 gui=NONE cterm=NONE
hi TabLineFill guifg=NONE ctermfg=NONE guibg=#344651 ctermbg=238 gui=NONE cterm=NONE
hi TabLineSel guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi Title guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi Visual guifg=NONE ctermfg=NONE guibg=#18263f ctermbg=235 gui=Bold cterm=Bold
hi VisualNOS guifg=NONE ctermfg=NONE guibg=#18263f ctermbg=235 gui=Bold cterm=Bold
hi WarningMsg guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi WildMenu guifg=#04040c ctermfg=232 guibg=#52dcdc ctermbg=80 gui=Bold cterm=Bold
hi Comment guifg=#647b85 ctermfg=66 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Constant guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi String guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Character guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Boolean guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Number guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Float guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Identifier guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Function guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Statement guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Conditional guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Operator guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Exception guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi PreProc guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Type guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Special guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi Underlined guifg=NONE ctermfg=NONE guibg=NONE ctermbg=NONE gui=underline cterm=underline
hi Error guifg=#fbfbe6 ctermfg=230 guibg=#f91212 ctermbg=9 gui=NONE cterm=NONE
hi Todo guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi CocErrorSign guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CocWarningSign guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CocHintSign guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi CocInfoSign guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssVendor guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssTagName guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssAttrComma guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssBackgroundProp guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssBorderProp guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssBoxProp guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssDimensionProp guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssFontProp guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssPositioningProp guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssTextProp guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssValueLength guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssValueInteger guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssValueNumber guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssIdentifier guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssIncludeKeyword guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssImportant guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssClassName guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssClassNameDot guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssProp guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssAttr guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssUnitDecorators guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssNoise guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi diffRemoved guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi diffChanged guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi diffAdded guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi diffSubname guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi elmDelimiter guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi elmOperator guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi FugitiveblameHash guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi FugitiveblameUncommitted guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi FugitiveblameTime guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi FugitiveblameNotCommittedYet guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitBranch guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitDiscardedType guifg=#c5152f ctermfg=160 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitSelectedType guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitHeader guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitUntrackedFile guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitDiscardedFile guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi gitcommitSelectedFile guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi helpHyperTextEntry guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi helpHeadline guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi helpSectionDelim guifg=#647b85 ctermfg=66 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi helpNote guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptOperator guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptBraces guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptNull guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonEscape guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonNumber guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonBraces guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonNull guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonBoolean guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonKeywordMatch guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonQuote guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsonNoise guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownH1 guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi markdownHeadingRule guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi markdownHeadingDelimiter guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi markdownListMarker guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownBlockquote guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownRule guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownLinkText guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownLinkTextDelimiter guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownLinkDelimiter guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownIdDeclaration guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownAutomaticLink guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownUrl guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownUrlTitle guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownUrlDelimiter guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownUrlTitleDelimiter guifg=#715b2f ctermfg=58 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownCodeDelimiter guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownCode guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownEscape guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi markdownError guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeHelp guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeHelpKey guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeHelpCommand guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeHelpTitle guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeUp guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeCWD guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeOpenable guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi NERDTreeClosable guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi pugJavascriptOutputChar guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptParens guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptLogicSymbols guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptReserved guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptLabel guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptFuncName guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptCall guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptVariable guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptBinaryOp guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptAssign guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptObjectLabel guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptDotNotation guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptOperator guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptTernaryOp guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptTypeAnnotation guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptIdentifierName guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptArrowFuncArg guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptParamImpl guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptRepeat guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptStatementKeyword guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptAliasKeyword guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptInterfaceKeyword guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptTemplateSB guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptMemberOptionality guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptOptionalMark guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi typescriptUnaryOp guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi GitGutterAdd guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi GitGutterChange guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi GitGutterDelete guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi GitGutterChangeDelete guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptOpSymbols guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptParens guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptDocTags guifg=#349474 ctermfg=66 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptDocSeeTag guifg=#3c5894 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptBrowserObjects guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptDOMObjects guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javaScriptFuncArg guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsParensIfElse guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsObjectKey guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsRepeat guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsArrowFunction guifg=#45a892 ctermfg=72 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsFunctionKey guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsFuncName guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsObjectFuncName guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsNull guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsObjectColon guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsParens guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsFuncParens guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsFuncArgs guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsSpecial guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsTemplateBraces guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsGlobalObjects guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsGlobalNodeObjects guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsImport guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsExport guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsExportDefault guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsExportDefaultGroup guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi jsFrom guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plug2 guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugH2 guifg=#4abfb4 ctermfg=73 guibg=NONE ctermbg=NONE gui=Bold cterm=Bold
hi plugBracket guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugNumber guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugDash guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugStar guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugMessage guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugName guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugUpdate guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugEdge guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugSha guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi plugNotLoaded guifg=#c3193e ctermfg=125 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi link SignifySignAdd GitGutterAdd
hi link SignifySignDelete GitGutterDelete
hi link SignifySignDeleteFirstLine SignifySignDelete
hi link SignifySignChange GitGutterChange
hi link SignifySignChangeDelete GitGutterChangeDelete
hi stylusVariable guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi stylusClass guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi stylusClassChar guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi stylusId guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi stylusIdChar guifg=#49bec8 ctermfg=74 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi cssVisualVal guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi stylusImport guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi vimCommentString guifg=#715b2f ctermfg=58 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi vimCommentTitle guifg=#3c5894 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi vimError guifg=#fbfbe6 ctermfg=230 guibg=#f91212 ctermbg=9 gui=NONE cterm=NONE
hi xmlNamespace guifg=#fbc58b ctermfg=222 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi xmlAttribPunct guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi xmlProcessingDelim guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptOpSymbol guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptDocNotation guifg=#349474 ctermfg=66 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptDocNamedParamType guifg=#3c5894 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptDocParamName guifg=#715b2f ctermfg=58 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptDocParamType guifg=#3c5894 ctermfg=60 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptTemplateSB guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptRepeat guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptObjectLabelColon guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptObjectMethodName guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi javascriptFuncName guifg=#52dcdc ctermfg=80 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi yamlFlowString guifg=#f4f5c4 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi yamlFlowStringDelimiter guifg=#fbfbe6 ctermfg=230 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE
hi yamlKeyValueDelimiter guifg=#f91212 ctermfg=9 guibg=NONE ctermbg=NONE gui=NONE cterm=NONE

if exists('*term_setansicolors')
  let g:terminal_ansi_colors = repeat([0], 16)

endif

if has('nvim')
endif

" ===================================
" Generated by Estilo 1.5.0
" https://github.com/jacoborus/estilo
" ===================================
